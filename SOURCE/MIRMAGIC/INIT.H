/***********************************************************
* Mirror Magic -- McDuffin's Revenge                       *
*----------------------------------------------------------*
* (c) 1994-2001 Artsoft Entertainment                      *
*               Holger Schemel                             *
*               Detmolder Strasse 189                      *
*               33604 Bielefeld                            *
*               Germany                                    *
*               e-mail: info@artsoft.org                   *
*----------------------------------------------------------*
* init.h                                                   *
***********************************************************/

#ifndef INIT_H
#define INIT_H

#include "main.h"

void OpenAll(void);
void CloseAllAndExit(int);
void InitJoysticks(void);

void InitColor(void);

#endif
